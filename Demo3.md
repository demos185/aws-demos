## Demo Project:
CD - Deploy Application from Jenkins Pipeline on EC2 Instance (automatically with docker-compose)
## Technologies used:
AWS, Jenkins, Docker, Linux, Git, Java, Maven, DockerHub
## Project Decription:
* Install Docker Compose on AWS EC2 Instance
* Create docker-compose.yml file that deploys our web application image
* Configure Jenkins pipeline to deploy newly built image using Docker Compose on EC2 server
* Improvement: Extract multiple Linux commands that are executed on remote server into a separate shell script and execute the script from Jenkinsfile
## Description in details:
### Install Docker Compose on AWS EC2 Instance
__Step 1:__ Go to [docker documentation](https://docs.docker.com/compose/install/) and  install docker-compose
```sh
sudo yum update
sudo yum install docker-compose-plugin
```
### Create docker-compose.yml file that deploys our web application image
__Step 1:__ Write `docker-compose` file which contains our app and postgress
1. Write verion of `docker-compose`
```yaml
version: '3.8'
```
2. Assign services (our app from private repo and postgress) and add info about them:
#### Your app
* Name of your app(name it whatever you want) 

* Image URL
* Ports
```yaml
services:
    java-maven-app:
      image: your_image_name_here/my-app:version
      ports:
        - 8080:8080
```
#### Postgres
* Image URL
* Ports
* Envaronment variables
```yaml
postgres:
  image: postgres:13
  ports:
    - 5432:5432
  environment:
    - POSTGRES_PASSWORD=my-pwd
```
### Configure Jenkins pipeline to deploy newly built image using Docker Compose on EC2 server
__Step 1:__ Open your `Jenkinsfile`  and go straight `deploy` stage

We need `docker-compose` file on EC2 instance, so:

__1.__ Inside `sshagent` execute `scp` command for `docker-compose`
```sh
sh "scp -o StrictHostKeyChecking=no docker-compose.yaml your_EC2_url:/home/ec2-user"
```
__2.__ SSH to the server and execute docker compose command:
* Add variable for docker-compose commands (the same like docker commands)
```groovy
def composeCmd = "docker-compose -f docker-compose.yaml up --detach"
```
* Add command to connect to the server and run docker compose commands:
```sh
sh "ssh -o StrictHostKeyCheking=no ec2-user@public_ip_endpoint ${composeCmd}"
```
### Improvement: Extract multiple Linux commands that are executed on remote server into a separate shell script and execute the script from Jenkinsfile
#### Extract to Shell Script
__Step 1:__ Create shell(in the same place where your docker-compose file) script that called `server-cmds.sh` with docker compose commands:
```bash
#!/usr/bin/env bash

docker-cmopose -f docker-compose.yaml up --detach
echo "success"
``` 
__Step 2:__ Return to `Jenkisnfile` and define shell command 
```groovy
stage('deploy') {
    steps {
        script {
           echo 'deploying docker image to EC2...'

           def Shscript = "bash ./server-cmds.sh ${IMAGE_NAME}"
```
__Step 3:__  Use `scp`  to copy your script on server
```sh
sh "scp server-cmds.sh your_EC2_url:/home/ec2-user" 
```
#### Replace Docker Image with newly built version
__Step 1:__ Open `docker-compose` file and change hardcoded name of image on variable
```yaml
java-maven-app:
    image: ${IMAGE}
```
__Step 2:__ return to your shell script and export variable before all commands:
```sh
#!/usr/bin/env bash

export IMAGE=$1

docker-cmopose -f docker-compose.yaml up --detach
echo "success"
```
__Note:__  Define parameter

__Step 3:__ Add parameter into `Jenkinsfile`
```groovy
def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}"
```