## Demo Project:
CD - Deploy Application from Jenkins Pipeline to EC2 Instance (automatically with docker)
## Technologies used:
AWS, Jenkins, Docker, Linux, Git, Java, Maven, DockerHub
## Project Decription:
* Prepare AWS EC2 Instance for deployment (Install Docker)
* Create ssh key credentials for EC2 server on Jenkins
* Extend the previous CI pipeline with deploy step to ssh into the remote EC2 instance and deploy newly built image from Jenkins server
* Configure security group on EC2 Instance to allow access to our web application
## Description in details:
### Prepare AWS EC2 Instance for deployment (Install Docker)
__Note:__ You can repeat this step from `Demo1`
### Create ssh key credentials for EC2 server on Jenkins
__Step 1:__ Install SSH agent plugin in Jenkins UI

__Step 2:__ Create credentials for EC2
* Choose `SSH Username with privaty key`
* username: `ec2-user`
* Private key: `Enter directly`
* Paste `.pem` content

### Extend the previous CI pipeline with deploy step to ssh into the remote EC2 instance and deploy newly built image from Jenkins server
__Step 1:__ Open `Pipeline syntax` in your pipeline

__Step 2:__ Choose `sshagent: SSH Agent` and choose your credentials and generate pipeline script
__Note:__ You can use it directly in your `Jenkinsfile`

__Step 3:__  Modify `Jenkinsfile` and add the SSH agent plugin:
>* Paste generated syntax into `deploy` stage
>* inside syntax block write logic for connecting or SSH-ing into the server
>```groovy
>sh 'ssh -o StrictHostKeyCheking=no ec2-user@public_ip_endpoint'
>```
>__Note:__ Key `-o StrictHostKeyCheking=no` used for suppress popup
>
>* Create variable for docker comment and paste it in `sh` command
>```groovy
>def dockerCmd = 'run docker -p 3080:3080 -d your_app_name/my-app:version'
>```
>```groovy
>sh "ssh -o StrictHostKeyCheking=no ec2-user@public_ip_endpoint ${dockerCmd}"
>```
__Note:__ You need to execute `docker login` on server before starting pipeline
It must look like this:
```groovy
stage('deploy'){
    steps{
        script{
            def dockerCmd = 'run docker -p 3080:3080 -d your_app_name/my-app:version'


            sshagent(['ec2-server-key']){
                sh "ssh -o StrictHostKeyCheking=no ec2-user@public_ip_endpoint ${dockerCmd}"
                
            }
        }
    }
}
```
### Configure security group on EC2 Instance to allow access to our web application
__Step 1:__ Allow access from Jenkins IP adress
* Open security on AWS  and add Jenkins IP adress into SSH rule
__Note:__ Also change port `3000` in another rule on `3080` if you use same server from previous demo 

__Step 2:__ Run Jenkins Pipeline