## Demo Project:
Complete the CI/CD Pipeline (Docker-Compose, Dynamic versioning)
## Technologies used:
AWS, Jenkins, Docker, Linux, Git, Java, Maven, DockerHub
## Project Decription:
* CI step: Increment version
* CI step: Build artifact for Java Maven application
* CI step: Build and push Docker image to DockerHub
* CD step: Deploy new application version with DockerCompose
* CD step: Commit the version update
## Description in details:
### CI step: Increment version
__Step 1:__ Remove `environment block` in your `Jenkinsfile` and add new stage `increment version` before build stages

__Note:__ You can read about in [here](https://www.mojohaus.org/build-helper-maven-plugin/parse-version-mojo.html)
```groovy
stage('increment version') {
    steps {
        script {
            echo 'incrementing app version...'
            sh 'mvn build-helper:parse-version versions:set \
                -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                versions:commit'
        }
    }
}
```

### CI step: Build artifact for Java Maven application
__Step 1:__ Change `sh` line in `build app` block  with `mvn clean package` to __avoid__ problems with existing folder 
```groovy
stage('build app') {
    steps {
        script {
            echo "building the application..."
            sh 'mvn clean package'
        }
    }
}
```
### CI step: Build and push Docker image to DockerHub
__Step 1:__ In docker  stage pay attention on `docker build` and `docker push` commands - change hardcoded verion `jma-2.0` on variable `${IMAGE_NAME}`

__Note:__ We use double quotes in those lines because we have variables in code
```groovy
stage('build image') {
    steps {
        script {
            echo "building the docker image..."
            withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                sh "docker build -t YOUR_REPO_HERE:${IMAGE_NAME} ."
                sh "echo $PASS | docker login -u $USER --password-stdin"
                sh "docker push YOUR_REPO_HERE:${IMAGE_NAME}"
            }
        }
    }
}
```
__Step 2:__ Return to `increment version` block and add new versions variable in the end of block of code
__Note:__ We use regular expression because we don't know correct version of app
```groovy
def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
```
__Step 3:__ Because this is a matcher, this will try to match every single version tag it can find inside the pom.xml and save it into an array. This give us another array with version tag and its children elements  

```groovy
def version = matcher[0][1]
```
__Step 4:__  Assign environment in the same block
```groovy
env.IMAGE_NAME = "$version-$BUILD_NUMBER"
```
>#### Replace new version in Dockerfile
>__Step 1:__  Open `Dockefile ` and configure it for dynamic tag
>* Change in `COPY` line this part `java-maven-app-1.1.0-SNAPSHOT.jar` on this `java-maven-app-*.jar`. Because older version doesn't work anymore. It let you change version dynamically
>```yaml
>COPY ./target/java-maven-app-*.jar /usr/app/
>```
>* Change `ENTRYPOINT` string because it doesn't work this way (regular expressions) on command
>```yaml
>CMD java -jar java-maven-app-*.jar 
>```
### Configure CI step: Commit version update of Jenkins back to Git repository
__Step 1:__ Create new stage in `Jenkinsfile` after all stages (build app,build iamge, deploy) that called `commit version update`
```groovy
stage ('commit version update') {
    steps {
        script {

        }
    }
}
```
__Step 2:__ Add excess credentials to git repository `gitlab-credentials`
```groovy
script {
    withcredentials ([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {

    }
}
```
__Step 3:__ Inside this block execute commands
```groovy
withcredentials ([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
    sh 'git add .'
    sh 'git commit -m "CI: version bump"'
    sh 'git push origin  HEAD:jenkins-job'
}
```
__Step 4:__ Tell Jenkins what is  origin - set origin adress
```sh
    sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/nanuchi/java-maven-app.git"
    sh 'git add .'
    sh 'git commit -m "CI: version bump"'
    sh 'git push origin  HEAD:jenkins-job'
```
__Step 5:__ Add  information about Git repository 

### CD step: Deploy new application version with DockerCompose
__Step 1:__ We use variables for docker-compose like `${Shscript}` and `${IMAGE_NAME}` so docker-compose already ready 

