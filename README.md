## 1. List of Demos
__1.__ Create and configure an EC2 Instance on AWS 

__2.__ CD - Deploy Application from Jenkins Pipeline to EC2 Instance (automatically with docker)

__3.__ CD - Deploy Application from Jenkins Pipeline on EC2 Instance (automatically with docker-compose)

__4.__ Complete the CI/CD Pipeline (Docker-Compose, Dynamic versioning)

__5.__ Interacting with AWS CLI
