## Demo Project:
Interacting with AWS CLI
## Technologies used:
AWS, Linux
## Project Decription:
* Install and configure AWS CLI tool to connect to our AWS account
* Create EC2 Instance using the AWS CLI with all necessary configurations like Security Group
* Create IAM resources like User, Group, Policy using the AWS CLI

## Description in details:
### Install and configure AWS CLI tool to connect to our AWS account
__Step 1:__ Install AWS CLI using [this guide](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
```sh
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
```
__Step 2:__ Configure AWS CLI: type `aws configure` and fulfill form
> 1. AWS Access Key ID:(You can download file for it from UI)
> 2. AWS Secret Access Key: (from the same file)
> 3. Default region name: You choose region where you will be create instances(You can see list of region in AWS UI ) E.g `eu-west-3`
> 4. Default output format: json

__Note:__ You can see this saved information here `~/.aws`
### Create EC2 Instance using the AWS CLI with all necessary configurations like Security Group
__Step 1:__ Create Security group: 

__Note:__ Security group needs to be created inside a VPC! 
```sh
aws ec2 create-security-group --group-name YOUR_SG_NAME_HERE --description "My-sg" --vpc-id YOUR_VPC_ID_HERE
```
__Note 1:__ If you want to see created groups: `aws ec2 describe-security-group`
__Note 2:__ If you want to see existing VPCs: `aws ec2 describe-vpcs`

__Step 2:__ Create firewall
```sh
aws ec2 authorize-security-group-ingress \ 
 --group-id YOUR_SG_NAME_HERE \
 --protocol tcp \
 --port 22 
 --cidr YOUR_IP_HERE/32
```
__Note:__ `--cidr`: here you assign IPs who have access to server

__Step 3:__ Create SSH key pair
```sh
aws ec2 create-key-pair \
--key-name MykpCli \
--query 'KeyMaterial' \
--output text > MykpCli.pem

```
__`--key-name`__ You can name it whatever you want

__Step 4:__ Create  EC2 instance
1. Fulfill this command with your data creatd above
```sh
aws ec2 run-instances \
--image-id ami-xxxxxxxx \
--count 1 \
--instance-type t2.micro \
--key-name MyKeyPair \
--security-group-ids sg-903004f8 \
--subnet-id subnet-6e7f829e 
```
__`subnet-id`__ You can see it in AWS UI or using `aws ec2 describe-subnets`
__`--image-id`__ You can see it in AWS UI

2. You can see all info about created instances using this command `awc ec2 describe-instances` and take IP-address from here

3. Change permissions of `.pem` file
```sh
chmod 400 YOUR_PEM_FILE.pem
```
5. Now you can SSH to the server
```
ssh -i MyKpCli.pem ec2-user@IP_OF_EC2_INSTANCE
```

### Create IAM resources like User, Group, Policy using the AWS CLI
__Step 1:__ Create user group
```sh
aws iam create-group --group-name YOUR_GROUP_NAME
```
__Step 2:__ Create user
```sh
aws iam create-user --user-name YOUR_USER_NAME
```
__Step 3:__ Add user to group
```sh
aws iam add-user-to-group --user-name YOUR_USER_NAME --group-name YOUR_GROUP_NAME
```
__Note:__ You can see group info using `aws iam get-group --group-name YOUR_GROUP_NAME`

__Step 4:__ Give permission for EC2 service
```
aws iam attach-group-policy --group-name YOUR_GROUP_NAME --policy-arn arn:aws:iam::aws:policy/AmazonEC2FullAccess 
```
__In this example__ we use `AmazonEC2FullAccess` for this group

* __Note 1:__ You can get list of ARN policies from AWS UI.

* __Note 2:__ You can see list of attached policies using this command
```sh
aws iam list-attached-group-policies --group-name YOUR_GROUP_NAME
```

__Step 5:__ Create Credentials for new User
__1.__ Create Password 
```sh
aws iam create-login-profile --user-name YOUR_USER_NAME --password YOUR_PASSWORD_FOR_USER --password-reset-required
```
__`--password-reset-required`__ This option gives you change password on  first login 

__Step 6:__ Create Policy and assign to Group

1. Let's go and check on UI AmazonEC2FullAccess policy ARN

2. Create policy
```sh
aws iam create-policy --policy-name YOUR_POLICY_NAME --policy-document file://YOUR_POLICY_FILE_NAME.json
```

3. Assign policy to group 
```sh
aws iam attach-group-policy --group-name YOUR_GROUP_NAME --policy-arn {policy-arn}
```
__`{policy-arn}`__ is arn line in policy file    

__Step 7:__  Now you can login with this user

__Note:__ You can get `User-ID` using this command and in `Arn` line see `"arn:aws:iam::YOUR_ID_HERE:user/"`
```sh
aws iam get-user --user-name YOUR_USER_NAME
```
__Step 8:__ Create Access Keys for new User
1. Create Access key for user
```sh
aws iam create-access-key --user-name YOUR_USER_NAME
```
2. Save  `AccessKeyId` and `SecretAccessKey`  for future usage