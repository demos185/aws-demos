## Demo Project:
Create and configure an EC2 Instance on AWS 
## Technologies used:
AWS, Docker, Linux
## Project Decription:
* Create and configure an EC2 Instance on AWS
* Install Docker on remote EC2 Instance
* Deploy Docker image from private Docker repository on EC2 Instance
## Description in details:
### Create and configure an EC2 Instance on AWS
__Step 1:__ Open AWS UI  and open EC2 and `launch instance` 

__Step 2:__ Select Distribution (in this case `Amazon Linux 2 AMI`)

__Step 3:__ Select instance type

__Step 4:__ Configure instance details(how many instances, network, turn on Public access)

__Step 5:__ Storage part (use default)

* __Optional__: You can configure tags

__Step 6:__ Configure Security Group - change SSH rule just for your IP adress(for example)

* __Note__ You can add configurations in Security group later(e.g open ports)

__Step 7:__ Choose key pair for instance or create a new key and dowload it

__Step 8:__ securely store key pair:
* Move the key to `~/.ssh/`

__Step 9:__ change permissions on that file
```sh
chmod 400 .ssh/your_private_key_pair.pem
```
__Note:__ If you don't do that you will get a warning or an error

__Step 10:__ Conect to the server via SSH
```sh
ssh -i ~/.ssh/your_private_key_pair.pem ec2-user@your_EC2_server_IP
```
### Install Docker on remote EC2 Instance
__Step 1:__ Use `yum update` with `sudo` to update state of repositories 

__Step 2:__ Install Docker
```sh
sudo yum install docker
```
__Step 3:__ Start Docker Daemon
```sh
sudo service docker start
```
__Step 4:__ Change permissions for docker
```sh
sudo usermod -aG docker $USER
```
__Step 5:__ Relogin on server to add changes on server
### Deploy Docker image from private Docker repository on EC2 Instance
__Note:__ Used [application](https://github.com/nanuchi/react-nodejs-example)

__Step 1:__ Log in into your Dockerhub

__Step 2:__ Pull image from your repository 
```sh
docker pull your_repo/my-app:1.0 
```
__Step 3:__ Run image:
* Run in a detached mode
* Define port 

```sh
docker run -d -p 3000:3080 image_name:verion  
```
__Step 4:__ Return to AWS and open `3000` port in `inbound rules`
__Step 5:__ Now you can open the app in your browser